//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SrewGTS400.rc
//
#define IDD_SREWGTS400_DIALOG           102
#define IDR_HTML_MAINDIALOG             103
#define IDR_MAINFRAME                   128
#define IDD_INPUTDIALOG                 129
#define IDD_MAIN                        131
#define IDC_Login                       1001
#define IDC_user                        1003
#define IDC_passwrd                     1004
#define IDC_filename                    1004
#define IDC_BUTTON3                     1005
#define IDC_BUTTON1                     1006
#define IDC_BUTTON4                     1006
#define IDC_DstVel                      1007
#define IDC_BUTTON5                     1007
#define IDC_BUTTON2                     1008
#define IDC_StartVel                    1009
#define IDC_Acc                         1010
#define IDC_DstVel1                     1011
#define IDC_StartVel1                   1012
#define IDC_Acc1                        1013
#define IDC_Dalay1                      1014
#define IDC_Dalay2                      1015
#define IDC_Dalay3                      1016
#define IDC_LIST1                       1017
#define IDC_modify                      1018
#define IDC_LIST2                       1019
#define IDC_password                    1020
#define IDC_level                       1021
#define IDC_BUTTON7                     1022
#define IDC_delete                      1023
#define IDC_BUTTON6                     1024
#define IDC_BUTTON8                     1025
#define IDC_delete2                     1026
#define IDC_delete3                     1027
#define IDC_BUTTON9                     1028
#define IDC_BUTTON10                    1029
#define IDC_BUTTON11                    1030
#define IDC_BUTTON12                    1031

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
